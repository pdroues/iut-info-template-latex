\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ups-iut-info-rapport}[20224/04/07 class for iut info tlse3 repport]

\LoadClass[11pt, a4paper, french]{article}

\DeclareOption{stage}{\gdef\@type{stage}}
\DeclareOption{alternance}{\gdef\@type{alternance}}
\DeclareOption{confidential}{\gdef\@confidential{true}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ExecuteOptions{stage}

\ProcessOptions\relax

% --------------------------------------------------------------------------------------- %

\PassOptionsToPackage{T1}{fontenc}
\PassOptionsToPackage{french}{babel}
\PassOptionsToPackage{top=1.5cm, bottom=2cm, left=2cm, right=2cm}{geometry}
\PassOptionsToPackage{colorlinks=true, linkcolor=black,  urlcolor=blue, citecolor=blue}{hyperref}
\PassOptionsToPackage{all, defaultlines=3}{nowidow}
\PassOptionsToPackage{defernumbers=true}{biblatex}

\RequirePackage{babel}          % la langue et convention associé
\RequirePackage{csquotes}       % necessaire a babel on dirrait
\RequirePackage{fontenc}        % l'encodeage
\RequirePackage{palatino}       % la font
\RequirePackage{geometry}       % les marges
\RequirePackage{ragged2e}       % justifier le texte
\RequirePackage{graphicx}       % les images
\RequirePackage{subcaption}     % pour les sous figures
\RequirePackage{float}          % les images a l'endroit exacte
\RequirePackage{xcolor}         % les couleurs
\RequirePackage{tikz}           % pour la page de garde 
\RequirePackage{fancyhdr}       % le style de la page
\RequirePackage{glossaries}     % le glossaire + acronyme
\RequirePackage{biblatex}       % bibliographie
\RequirePackage{tocloft}        % le sommaire
\RequirePackage{hyperref}       % les liens
\RequirePackage{parskip}        % les interlignes
\RequirePackage{ifthen}         % Les conditions
\RequirePackage{wallpaper}      % image de fond de la première page
\RequirePackage{nowidow}        % pour les lignes orphelines

% --------------------------------------------------------------------------------------- %

\gdef\@backgroundfile{}
\gdef\@enterprisestampfile{}
\gdef\@enterprisesignaturefile{}
\newboolean{bottomimage}

% --------------------------------------------------------------------------------------- %

\justifying

\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\fancyfoot[R]{\thepage}
\fancyfoot[L]{\includevariableimage{\@iutlogofile}{1.3cm}}

\@ifclasswith{ups-iut-info-rapport}{confidential}{\fancyhead[C]{\textcolor{red}{Confidentiel}}}{}
\@ifclasswith{ups-iut-info-rapport}{confidential}{\renewcommand{\headrulewidth}{0.4pt}}{}

\setlength{\headheight}{15pt}
\setlength{\footskip}{42pt}

% --------------------------------------------------------------------------------------- %

\newcommand{\setauthor}[1]{\author{#1}}
\newcommand{\setiuttutor}[1]{\gdef\@iuttutor{#1}}
\newcommand{\setenterprisetutor}[1]{\gdef\@enterprisetutor{#1}}

\newcommand{\setstartdate}[1]{\gdef\@setstartdate{#1}}
\newcommand{\setenddate}[1]{\gdef\@setenddate{#1}}

\newcommand{\setiutlogofile}[1]{\gdef\@iutlogofile{#1}}
\newcommand{\setenterpriselogofile}[1]{\gdef\@enterpriselogofile{#1}}
\newcommand{\setenterprisestampfile}[1]{\gdef\@enterprisestampfile{#1}}
\newcommand{\setenterprisesignaturefile}[1]{\gdef\@enterprisesignaturefile{#1}}

\newcommand{\setbackgroundfile}[1]{\gdef\@backgroundfile{#1}}

\newcommand{\includevariableimage}[2]{\includegraphics[height={#2}]{#1}}

\newcommand{\maketitlepage}[1]{
	\begin{titlepage}

        \ifthenelse{\equal{#1}{background}}{
            \ifx\@backgroundfile\@empty
            \else
                \ThisCenterWallPaper{0.8}{\@backgroundfile}
            \fi
        }{}
        
        \begin{tikzpicture}[remember picture, overlay]
        
            \node [anchor=north west, xshift=1cm, yshift=-1cm] at (current page.north west) {
                \begin{minipage}[t]{0.45\textwidth}
                    \includevariableimage{\@iutlogofile}{3cm}
                \end{minipage}
            };
        
            \node [anchor=north east, xshift=1cm, yshift=-1cm] at (current page.north east) {
                \begin{minipage}[t]{0.45\textwidth}
                    Auteur du rapport: \\
                    \textbf{{\fontsize{16pt}{14pt}\selectfont \@author}}\\
        			Tuteur IUT:\\
                    \textbf{{\fontsize{16pt}{14pt}\selectfont \@iuttutor}}\\
           			\@ifclasswith{ups-iut-info-rapport}{stage}{Date du stage:\\}{}
        			\@ifclasswith{ups-iut-info-rapport}{alternance}{Date de l'alternance:\\}{}
                    \texttt{\textbf{\@setstartdate}}\ au \texttt{\textbf{\@setenddate}}\\
                \end{minipage}
            };

            \node [align=center, anchor=center, yshift=3cm] at (current page.center) {
                \Large
                \@ifclasswith{ups-iut-info-rapport}{stage}{RAPPORT DE STAGE}{}
                \Large
                \@ifclasswith{ups-iut-info-rapport}{alternance}{RAPPORT D'ALTERNANCE}{}
                \\ \\ \\
                \Huge
                \@title
            };

            \node [align=center, anchor=center, yshift=-8cm] at (current page.center) {
                \Large
                \@ifclasswith{ups-iut-info-rapport}{confidential}{\textcolor{red}{CONFIDENTIEL}}{}
            };

            \node [anchor=south west, xshift=1cm, yshift=1cm] at (current page.south west) {
                \begin{minipage}[t]{0.45\textwidth}
        			\includevariableimage{\@enterpriselogofile}{3cm}\\
                    \ifthenelse{\equal{#1}{background}}{
                        \ifx\@enterprisestampfile\@empty
                            \\ \\ \\ \\ \\
                        \else
                            \includevariableimage{\@enterprisestampfile}{3cm}\\
                        \fi
                    }{}
        			TUTEUR:\\
                    \textbf{{\fontsize{16pt}{14pt}\selectfont \@enterprisetutor}}\\
                    \ifthenelse{\equal{#1}{background}}{
                        \ifx\@enterprisesignaturefile\@empty
                            \\ \\ \\ \\ \\
                        \else
                            \includevariableimage{\@enterprisesignaturefile}{3cm}\\
                        \fi
                    }{}
                \end{minipage}
            };

        \end{tikzpicture}

	\end{titlepage}
}

\newcommand{\blankpage}{
    \clearpage
    \thispagestyle{empty}
    \null
    \clearpage
}

\newcommand{\initdocument}{
    \maketitlepage{background}
    \blankpage
    \maketitlepage{}
}